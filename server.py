from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.modules import CanvasGrid, ChartModule, TextElement
from mesa.visualization.UserParam import UserSettableParameter
from model import Covid
from model import State

class InfectedElement(TextElement):
    def __init__(self):
        pass

    def render(self, model):
        return "Infected agents: " + str(model.infections)

def covid_draw(agent):
    # 1
    if agent is None:
        return
    portrayal = {"Shape": "circle", "r": 0.5, "Filled": "true", "Layer": 0}

    # 2
    if agent.state is State.INFECTED : 
        portrayal["Color"] = ["crimson", "crimson"]
        portrayal["stroke_color"] = "crimson"
    else:
        portrayal["Color"] = ["limegreen", "limegreen"] 
        portrayal["stroke_color"] = "limegreen"
    return portrayal

# 3
infected_element = InfectedElement()
canvas_element = CanvasGrid(covid_draw, 29, 29, 500, 500)
infection_chart = ChartModule([
    {"Label": "Total of Infections", "Color": "Black"}, 
    #{"Label": "Daily", "Color": "Blue"}, 
    ])

# 4
model_params = {
    "height": 29,
    "width": 29,
    "infection_range_min": UserSettableParameter("slider", "Infection Range Minimum", 0.5, 0.0, 1.0, 0.1),
    "infection_range_max": UserSettableParameter("slider", "Infection Range Maximum", 0.9, 0.0, 1.0, 0.1),
    "social_distancing_quota": UserSettableParameter("slider", "Social Distancing", 1, 0.00, 1.0, 0.1),
}

# 5
server = ModularServer(Covid,
                    [canvas_element, infected_element, infection_chart],
                    "Covid", model_params)
