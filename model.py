from mesa import Agent, Model
from mesa.space import Grid
from mesa.time import SimultaneousActivation
from mesa.datacollection import DataCollector
import numpy as np
import random
from enum import IntEnum

class State(IntEnum):
    HEALTHY = 0
    INFECTED = 1
    RECOVERED = 2

class Radius(IntEnum):
    MIN_RADIUS = 1
    MAX_RADIUS = 5

class CovidAgent(Agent):

    #Initialization
    def __init__(self, pos, model, state, has_infected_neighbor, movement_radius):
        super().__init__(pos, model)

        self.pos = pos #position on the grid
        self.state = state #0 : healthy, 1 : infected, 2 : recovered
        self.movement_radius = movement_radius #radius where the agent can move and enter in contact with other agents
        self.has_infected_neighbor = has_infected_neighbor #true if there's someone infected within the agent's radius

        #A high infection probability means the agent has a weak system and/or is not wearing a mask and therefore very susceptible
        #Wheras a low probability means a strong immune system and/or wears masks
        self.infection_probability = np.random.uniform(model.infection_range_min, model.infection_range_max)



    #Step function
    def step(self):

        #get all neighbors
        neighbors = self.model.grid.iter_neighbors(self.pos, moore = False, include_center = False, radius=self.movement_radius)
        
        #check if agent has an infected neighbor
        for neighbor in neighbors:
                
                if (neighbor.state == State.INFECTED):
                #if (neighbor.is_infected == 1):
                    self.has_infected_neighbor = True
                    
    #Advance function
    def advance(self):
        if (self.has_infected_neighbor and self.state == State.HEALTHY):

            #neighbor infects agent with a probability p (uniformly distributed )
            gets_infected = np.random.choice([0,1], p=[1 - self.infection_probability,self.infection_probability])
            
            if (gets_infected == 1):
                self.state = State.INFECTED
                self.model.infections += 1
                self.model.daily_infections += 1

class Covid(Model):

    #Initialization
    def __init__(self, height, width, infection_range_min, infection_range_max, social_distancing_quota):
        
        #Variables Setup
        
        #Population
        self.height = height
        self.width = width
        total_agents = self.height*self.width

        #Set to default value if the range intervall is not valid
        if (infection_range_max < infection_range_min):
            infection_range_max = 0
            infection_range_min = 0
            #INFORM THE USER THAT AN ERROR OCCURED 

        #The range of probability describe how infectious the disease is 
        # and/or how many people are taking their precautions
        self.infection_range_min = infection_range_min 
        self.infection_range_max = infection_range_max

        #Radius in which the people can move 
        self.social_distancing_quota = social_distancing_quota
        total_social_distancing_agents = int(total_agents * social_distancing_quota)

        #Grid and Scheduler
        self.grid = Grid(height, width, False)
        self.schedule = SimultaneousActivation(self)

        #Data Collector
        self.infections = 1
        self.daily_infections = 1

        self.datacollector = DataCollector(
            {"Total of Infections" : "infections", 
             "Daily" : "daily_infections",
            },
            {"x": lambda a: a.pos[0], "y": lambda a: a.pos[1]}
        )  
        
        #For a quota = X (For example X = 0.3 (30%)) we generate 30% random x values and 30% random y values
        random_agents = [0]*total_social_distancing_agents

        for i in range(total_social_distancing_agents):
            x = np.random.randint(width)
            y = np.random.randint(height)

            while ((x,y) in random_agents):
                x = np.random.randint(width)
                y = np.random.randint(height)
            
            random_agents[i] = (x,y)

        #Agents Setup : per default only the middle agent is infected and all the others are healthy
        for cell in self.grid.coord_iter():
            x = cell[1]
            y = cell[2]
            if (y == int(height/2) and x == int(width/2)): 
                agent_state = State.INFECTED
            else :
                agent_state = State.HEALTHY

            #per default all agents move freely (no social distancing)
            agent_radius = Radius.MAX_RADIUS

            #setting the agent's raidus depending on whether or not it follows social distancing
            if (x,y) in random_agents: 
                    agent_radius = Radius.MIN_RADIUS

            #Add agent to the model
            agent = CovidAgent((x, y), self, agent_state, False, agent_radius)
            self.grid.place_agent(agent, (x, y))
            self.schedule.add(agent)

        self.running = True
        self.datacollector.collect(self)

    def step(self):
        #Rest counter of infected agents and start gives the number of infections PER time-step
        self.daily_infections = 0 
        self.schedule.step()

        #Collect Data
        self.datacollector.collect(self)

        #Stop model if all agents are infected 
        if self.infections == self.schedule.get_agent_count():
            self.running = False
