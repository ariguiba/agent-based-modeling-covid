An Agent-based modeling Application of the effect of mobility on the spread of the disease "Covid-19"
=========================================

Introduction of the simulation 
------------
For a better understanding of the agent-based model, we examine in the following part
one specific application of ABM, namely a simulation of the spread of an infectious
disease within a population and the effect of mobility restrictions on the speed of the
infection. In this simulation, our agents represent human beings, whose state can be
healthy (green) or infected (red). They also have an infection index, which describes
the viral load, when an agent is infected : A low infection index would mean the agent
doesn’t have a big viral load, and thus is less likely to infect another agent, when it
interacts with it,

The simulation doesn’t represent the explicit movements of the agents, but rather we
assume the agents are moving within their radius, and we note the change of states.

![Initial State of the Simulation](pictures/simulation.png?raw=true)


The legend is as following.

1 Parameters : infection probability interval and share of agents applying social dis-
tancing.

2 Spacial grid : environment where agents are located and where they can interact.

3 Media control elements : To start, stop, reset the simulation and to run it one
time-step at a time.


Using Mesa
------------

`Mesa` is an Apache2 licensed agent-based modeling (or ABM) framework in Python.

It allows users to quickly create agent-based models using built-in core components (such as spatial grids and agent schedulers) or customized implementations; visualize them using a browser-based interface; and analyze their results using Python's data analysis tools.

Getting started quickly:

````
    $ pip install mesa
````

You can also use `pip` to install the github version:

````
    $ pip install -e git+https://github.com/projectmesa/mesa#egg=mesa

````

The full documentation can be found ![here](http://mesa.readthedocs.org/en/master/)